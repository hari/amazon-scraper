var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');

var captcha = require('./src/captcha');
var goto = require('./src/goto').goto;
var parse = require('./src/goto').parse
var profile = require('./src/profile');
var gotoProfiles = require('./src/auth');
var search = require('./src/search');
//how many times should the request be called for empty reviews collected
const MAX_TRY = 5;
const FILE_OUTPUT = "emails.csv";
const POOL_SIZE = 10;
//how manny links to visit in one batch
//after visting this number of links
//the script will wait for REST_TIME
const BATCH_SIZE = 200;
const REST_TIME = 60; //in seconds
var queue = [];
var active = [];
var shouldScrape = 0;

var html = '<!doctype html><html><head><title>Amazono</title><script type="text/javascript">setTimeout(function() { document.location = document.location }, 5000);</script></head><body>%s</body></html>';

var server = express();
var started = false;
var limit = -1;
var scraped = {
    url: 0,
    profiles: 0,
    reviews: 0
}

server.use(bodyParser.urlencoded({
    extended: true
}));

server.get('/', function (req, res) {
    if (started) {
        res.redirect('/status');
    } else {
        res.sendFile(path.join(__dirname, 'index.html'));
    }
});

server.get('/emails', function (req, res) {
    if (fs.existsSync(FILE_OUTPUT)) {
        fs.readFile(FILE_OUTPUT, function (error, data) {
            if (error) {
                return res.send('Error!');
            }
            res.send(html.replace("%s", '<h1>Emails</h1>' + data.toString().split('\n').map(function (item) {
                return '<div>' + item + '</div>';
            }).join('')));
        })
    } else {
        res.send(html.replace("%s", 'No emails so far!'));
    }
});

server.get('/status', function (req, res) {
    var msg = started ? '<p>The main script is running.</p>' : '<p>The main script is not running. Background tasks may be going on.</p>';
    res.send(html.replace('%s', '<div>URLs Scraped: <strong>' + scraped.url + '</strong>, Reviewers Collected: <strong>' + scraped.reviews + '</strong>, Profile seen: <strong>' + scraped.profiles + '</strong></div>'));
})

server.get('/stop', function (req, res) {
    if (!started) {
        res.redirect('/');
    } else {
        stop();
        res.send(html.replace('%s', 'Task has been stopped. Redirecting to home page.'));
    }
});

server.get('/solve', function (req, res) {
    console.log(req.query);
    captcha(req.query.link, function (err, data) {
        if (err) {
            res.send(err);
        } else {
            res.send(data);
        }
    })
})

server.post('/', function (req, res) {
    saveProxies(req.body.proxy);
    saveUrls(req.body.url);
    saveAccounts(req.body.account);
    limit = req.body.limit;
    res.send(html.replace('%s', 'No worries!, Starting job if not already Started.'));
    if (!started) {
        //start scarping
        scraped.url = 0;
        scraped.reviews = 0;
        scraped.profiles = 0;
        if (!fs.existsSync(FILE_OUTPUT)) {
            fs.writeFileSync(FILE_OUTPUT, '');
        }
        initActive();
        queue = [];
        startScraping(getNextUrl());
    }
})

server.post('/search', function (req, res) {
    search(req.body.q, req.body.p, function (items) {
        res.send(items);
    });
});

server.get('/search', function (req, res) {
    res.sendFile(path.join(__dirname, 'search.html'));
});

server.set('port', process.env.PORT || 5000)
server.listen(server.get('port'), function () {
    console.log('Running at port:' + server.get('port'));
});

function stop() {
    scraped.url = 0;
    scraped.reviews = 0;
    scraped.profiles = 0;
    initActive();
    limit = -1;
    queue = [];
    started = false;
    working = false;
}

function saveUrls(urls) {
    fs.writeFileSync('urls.txt', urls);
}

function saveAccounts(accounts) {
    fs.writeFileSync('auths.txt', accounts);
}

function saveProxies(proxies) {
    fs.writeFileSync('proxies.txt', proxies);
}

function getNextUrl() {
    var urls = fs.readFileSync('urls.txt').toString().split('\n');
    if (urls.length > 0) {
        var url = urls.pop().trim();
        saveUrls(urls.join('\n'));
        return url;
    }
    return null;
}

function getLinksFromFile() {
    if (!fs.existsSync('links.txt')) return [];
    var links = fs.readFileSync('links.txt').toString().split('\n');
    var min = Math.min(BATCH_SIZE, links.length);
    fs.writeFileSync('links.txt', links.slice(min).join('\n'));
    return links.slice(0, min);
}

function saveQueue() {
    fs.writeFileSync('links.txt', queue.slice(BATCH_SIZE).join('\n'));
    queue = queue.slice(BATCH_SIZE);
}

function startScraping(url) {
    if (reachedLimit()) return;
    if (url == null || url == "") {
        started = false;
        //call one last time.
        queueCall([]);
        working = false;
        console.log('Everything done! [waiting all profiles to be visited]');
        return;
    }
    shouldScrape = 0;
    scraped.url++;
    started = true;
    console.log('Scraping: ' + url);
    var asin = url.split('/ref')[0];
    asin = asin.substring(asin.lastIndexOf('/') + 1);
    writeOutput([url]);
    recursiveCall(asin, 1);
}

function writeOutput(lines) {
    if (lines.length == 0) {
        return;
    }
    fs.appendFile(FILE_OUTPUT, '\n' + lines.join('\n'), function (err) {

    })
}

function recursiveCall(asin, pageNumber) {
    if (reachedLimit()) return;
    goto(pageNumber, asin, function (error, result) {
        if (error) {
            recursiveCall(asin, pageNumber);
            return console.log('Goto: ' + error);
        }
        var links = parse(result);
        if (links.length == 0) {
            shouldScrape++;
        }
        //no more reviews left, assuming end.
        if (shouldScrape <= MAX_TRY) {
            console.log('Done page: ' + pageNumber);
            pageNumber++;
            scraped.reviews += links.length;
            queueCall(links);
            recursiveCall(asin, pageNumber);
        } else {
            console.log('Done scraping reviewers of [' + asin + ']');
            startScraping(getNextUrl());
        }
    });
}
var working = false;

function queueCall(links) {
    queue = queue.concat(links);
    if (queue.length >= BATCH_SIZE) {
        saveQueue();
    }
    if (links.length == 0 && queue.length == 0) {
        links = getLinksFromFile();
        return (links.length === 0) ? console.log('Queue Empty.') :
            setTimeout(function () {
                queueCall(links);
            }, REST_TIME * 1000);
    }
    if (queue.length < POOL_SIZE || working) {
        //new links are being added
        //we will wait until the queue is full
        //else it will iterate and visit profile
        return;
    }
    working = true;
    var limit = queue.length > POOL_SIZE ? POOL_SIZE : queue.length;
    var profiles = [];
    var indicies = [];
    for (var ui = 0; ui < limit; ui++) {
        if (!active[ui]) {
            active[ui] = true;
            var url = queue.pop();
            if (typeof (url) == 'string' && url.length > 4 && url.substring(0, 4) == 'http') {
                //collectEmail(asin, url.trim(), ui);
                profiles.push({
                    link: url.trim(),
                    index: ui
                });
                indicies.push(ui);
            } else {
                active[ui] = false;
            }
        }
    }
    collectFromProfiles(profiles, indicies);
}

function collectFromProfiles(profiles, indicies) {
    if (profiles.length == 0 || reachedLimit()) {
        return resetActive(indicies);
    }
    gotoProfiles(profiles,
        function (index) {
            if (index == -1 || index >= active.length) return;
            active[index] = false;
            scraped.profiles++;
        },
        function (profilesCollection, profileEmails) {
            working = false;
            if (profileEmails.length > 0) {
                writeOutput(profileEmails);
            }
            if (profilesCollection.length == 0) {
                setTimeout(function () {
                    queueCall([]);
                }, 10000); //wait 10s before making new request.
            } else {
                resetActive(profilesCollection.map(function (p) {
                    return p.index;
                }));
                queue = queue.concat(profilesCollection);
                queueCall([]);
            }
        });
}

function resetActive(indicies) {
    for (var i = 0; i < indicies.length; i++) {
        active[indicies[i]] = false;
    }
}

function initActive() {
    for (var i = 0; i < POOL_SIZE; i++) {
        active[i] = false;
    }
}

function reachedLimit() {
    if (fs.readFileSync(FILE_OUTPUT).toString().split('\n').length == limit) {
        console.log('Reached Limit!');
        return true;
    }
    return false;
}