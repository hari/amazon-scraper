var request = require('request');
var getProxy = require('./nmi');
var fs = require('fs');
var r = request.defaults({
  jar: true
});

function gotoPage(pageNumber, asin, callBack) {
  r.post({
    url: 'https://www.amazon.com/ss/customer-reviews/ajax/reviews/get/ref=cm_cr_arp_d_paging_btm_2',
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    form: {
      sortBy: 'recent',
      reviewerType: 'all_reviews',
      formatType: '',
      filterByStar: 'positive',
      pageNumber: pageNumber,
      filterByKeyword: '',
      shouldAppend: 'undefined',
      deviceType: 'desktop',
      reftag: 'cm_cr_arp_d_paging_btm_2',
      pageSize: '10',
      asin: asin,
      scope: 'reviewsAjax' + (parseInt(pageNumber) - 1)
    }
  }, function (error, response, body) {
    if (error) {
      callBack(error, null);
    } else {
      callBack(null, body);
    }
  });
}

function parseToCSV(raw) {
  var uri = 'https://www.amazon.com/gp/pdp/profile/%sie=UTF8';
  var res = [];
  if (raw == undefined) return res;
  raw = raw.split(/&&&/);
  for (var i = 0; i < raw.length; i++) {
    try {
      var parsed = JSON.parse(raw[i]);
      if (parsed[0] === 'append') {
        parsed = parsed[2].split('/gp/pdp/profile/')[1];
        res.push(uri.replace('%s', parsed.substring(0, parsed.indexOf('ie=UTF8'))));
      }
    } catch (ignore) {

    }
  }
  return res;
}

module.exports = {
  goto: gotoPage,
  parse: parseToCSV
};