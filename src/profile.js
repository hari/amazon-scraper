var getProxy = require('./nmi');
var request = require('request');
var fs = require('fs');

function linkScrapper(link, callBack) {
    request.get({
        jar: true,
        url: link,
        timeout: 120000,
        headers: {
            'Host': 'www.amazon.com',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
            'Accept': '*/*'
        }
    }, function (error, response, data) {
        if (error) {
            callBack(error, null);
        } else {
            var emails = [];
            if (data.indexOf('personalDescription') >= 0) {
                data = data.split('personalDescription')[1].split('headerMessage')[0];
                if (data != "" && data.indexOf('@') >= 0) {
                    emails = data.split('\n').join(' ').split(' ').filter(function (word) {
                        return /\S+@\S+\.\S+/.test(word);
                    });
                }
            }
            callBack(null, emails);
        }
    })
}

module.exports = linkScrapper;