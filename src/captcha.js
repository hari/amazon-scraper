const request = require('request');
const DBC = require('deathbycaptcha');
const dbc = new DBC("pimpao", "123123123");
var fs = require('fs');

function convertCaptcha(link, callback) {
  request(link).pipe(fs.createWriteStream('captcha.png')).on('close', function () {
    callDBC(callback);
  });
}

function callDBC(callback) {
  dbc.solve(fs.readFileSync('captcha.png'), function (err, id, solution) {
    callback(err, solution);
  });
}

module.exports = convertCaptcha;